var express = require("express");
var app = express();
var port = process.env.PORT || 3000;
var mongoose = require("mongoose");
var bodyParser = require("body-parser");
var GeneralList = require("./api/models/general-list-model");

mongoose.Promise = global.Promise;
//mongoose.connect("mongodb://localhost:27017");
mongoose.connect("mongodb://easy-my:Easy-My2800@@cluster0-shard-00-00-gqaas.mongodb.net:27017,cluster0-shard-00-01-gqaas.mongodb.net:27017,cluster0-shard-00-02-gqaas.mongodb.net:27017/easy-my?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require("./api/routes/general-list-route");
routes(app);

app.listen(port);
console.log("general-list-api RESTful server started on port: " + port);
