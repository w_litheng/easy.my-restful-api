module.exports = function(app) {
    var generalList = require("../controllers/general-list-controller");

    app.route("/items")
        .get(generalList.getAllItems)
        .post(generalList.createItem);

    app.route("/items/:itemId")
        .get(generalList.getItemById);
};
