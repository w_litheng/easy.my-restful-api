var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var GeneralListSchema = new Schema({
    Id: {
        type: Number
    },
    Title: {
        type: String
    },
    Price: {
        type: Number
    },
    Category: {
        type: [{
            type: String,
            enum: ["Electronics", "Books", "Fashion"]
        }],
    },
    CreatedDate: {
        type: Date,
        default: Date.now
    },
    CreatedBy: {
        type: String
    }
});

module.exports = mongoose.model("GeneralList", GeneralListSchema);
