var mongoose = require("mongoose");
var GeneralList = mongoose.model("GeneralList");

exports.getAllItems = function(request, response) {
    GeneralList.find({}, function(error, item) {
        if(error) {
            response.send(error);
        }

        response.json(item);
    });
}

exports.createItem = function(request, response){
    var newItem = new GeneralList(request.body);
    newItem.save(function(error, item){
        if(error){
            response.send(error);
        }

        response.json(item);
    });
}

exports.getItemById = function(request, response){
    GeneralList.findById(request.params.itemId, function(error, item){
        if(error){
            response.send(error);
        }

        response.json(item);
    });
}
